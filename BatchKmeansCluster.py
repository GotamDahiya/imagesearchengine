# importing the necessary libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import os, random, json, joblib

from config import config
from sklearn.cluster import MiniBatchKMeans
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler

def find_number_clusters(X):
    sse = []
    for i in range(1,11):
        model = MiniBatchKMeans(n_clusters=i,random_state=3,max_iter=100).fit(X)
        sse.append(model.inertia_)
        pass

    plt.plot(range(1,11), sse, marker="*")
    plt.xlabel("Number of clusters")
    plt.ylabel("SSE value")
    plt.show()
    pass


# reading the Encoded values from the CSV file ./output/encoded_values_train_val.csv
encoded_values = pd.read_csv(config.ENCODER_VALUES_PATH,header=None)

# printing the first 5 rows of the dataset
# print("[DEBUG] printing first five rows of encoded values")
# print(encoded_values.head())

# extracting the filenames from the dataset
filenames = encoded_values.loc[:,0]

# extracting the features from the dataset
features  = encoded_values.loc[:,1:]
features.columns = [i for i in range(0,64)]


# find_number_clusters(features)

print("[INFO] Creating KMeans model")
km = MiniBatchKMeans(n_clusters=3, random_state=42, max_iter=100).fit(features)

print("[INFO] Predict label and obtaning distance from cluster centers")
pred  = km.predict(features)
trans = np.max(km.transform(features),axis=1)

# print("[DEBUG]",end=" ")
# print(trans.shape, pred.shape)

pred_trans = np.vstack((pred, trans)).T

print("[INFO] Storing distance and cluster labels in a CSV file.")
output_file = open(config.KMEANS_CLUSTER_PATH,"w")

for (distance,filename) in zip(pred_trans, filenames):

    vec = ",".join([str(v) for v in distance])
    output_file.write("{},{}\n".format(filename,vec))
    pass

print("[INFO] Saving KMeans model")
joblib.dump(km, config.KMEANS_MODEL_PATH)

print("[INFO] finished clustering of data.")