import numpy as np
import json, os, cv2

from config import config

filenames_file = open("./output/SiameseNetwork/test.txt",'r')
file_arr = np.array([file.strip("\n") for file in filenames_file])

# print(file_arr)

l, i = file_arr.shape[0], 0

while i < l:

    img_path = os.path.abspath(os.path.join("./ImageSearch", file_arr[i]))
    
    img = cv2.imread(img_path)
    
    cv2.imshow("Image", img)

    key = cv2.waitKey(0) & 0xFF

    if key == ord("d"):
        try:
            i += 1
            file = file_arr[i]
        except:
            print("No more files to the right")
            continue
    elif key == ord('a'):
        try:
            i -= 1
            file = file_arr[i]
        except:
            print("No more files to the left")
            continue
    elif key == ord('q'):
        break