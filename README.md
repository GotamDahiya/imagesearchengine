# Image Search Engine

## Introduction <a name="introduction"></a>

The purpose of this project is to make an image search engine using Convolutional Neural Networks, Autoencoders, K-Means clustering and Siamese Networks.

The Convolutional Neural Networks(CNN) are used for extracting features from an image. Only fully convolutional are used for extracting the features. The extracted features are fed to the 

Autoencoders are used for extracting the relevant features after an image is passed through the CNN. This is done to reduce the complexity of the image by removing the redundant data, thereby increasing the accuracy of the clustering model.

K-Means Clustering is used for clustering the Autoencoder's extracted features into similar groups. the purpose behind this is that the similar image can be queried from the cluster itself or for using a Siamese Network within this cluster.

## Table of Contents

* [Introduction](#introduction)
* [Technologies](#technologies)
* [Description](#description)
    * [Convolutional Neural Network](#convolutional-neural-network)
    * [Autoencoder](#autoencoder)
    * [K-Means Clustering](#k-means-clustering)
    * [Siamese Network](#siamese-network)
* [Dataset](#dataset)
* [Updates](#updates)
* [Directories](#directories)
    * [config](#config)
    * [dataset](#dataset-train-test-val)
* [Setup](#setup)
* [Authors](#authors)

## Technologies <a name="technologies"></a>

Project is created with:
```
Python 3.8.2
Tensorflow 2.3.0
Numpy 1.18.5
Pandas 1.1.0
```

## Description <a name="description"></a>

### Convolutional Neural Network <a name="convolutional-neural-network"></a>

Images are first fed through a vanilla Convolutional Neural Network to extract features from them. The extracted features are stored in a CSV file along with the class labels. These features are then fed into a encoder of an Autoencoder.

MobileNet is used as it is 10 times faster and 27 time less compute intensive than VGG16. The metric for this layer is the time taken for generating the features of an image. In this regard MobileNet is better. Any of the CNN used for real-time object detections should be used, as they are faster and consume lesser resources.

This is done in *[CNN_feature_extraction](CNN_feature_extraction.ipynb)*.

At first *MobileNet* was used as it is not as computationally intensive as VGG16 and ResNet flavours. It created feature vectors with dimensions *7\*7\*1024* which were too large to load in memory even with a generator function. 

Therefore instead of *MobileNet*, *LeNet* architecture will be used or my own vanilla CNN model.

Currently *MobileNetV2* is being used for testing before training a custom made CNN model.

| ![MobileNet Body Architecture](./output/CNN-feature-extraction/MobileNetV2-Architecture.png) |
|:--:|
| *MobileNetV2 Architecture* |


### Autoencoder <a name="autoencoder"></a>

The features extracted using the CNN are fed into the Autoencoder. This is trained on the entire dataset present. Two methods will be tried for this part, one using fully connected layers or convolutional and maxpooling layers. The metric for comparing the two is the accuracy with which the features can be reconstructed with redundant data removed.

This part of the process increases the signal-to-noise ratio of the images, allowing images which are more similar to be grouped together with noise not causing them to be put in different groups.

The decoder present in the Autoencoder model is used for converting the relevant features into an image to be displayed to the user. This part is done after the Siamese network. The decoder's input is the output of the encoder model.

| ![Encoder Architecture](./output/Autoencoders/encoder_plot.png) |
|:--:|
| *Encoder Architecture* |


| ![Decoder Architecture](./output/Autoencoders/decoder_plot.png) |
|:--:|
| *Decoder Architecture* |


| ![Autoencoder Architecture](./output/Autoencoders/autoencoder_plot.png) |
|:--:|
| *Autoencoder Architecture* |

### K-Means Clustering <a name="k-means-clustering"></a>

The relevant features extracted from the above step are passed through a K-Means Clustering algorithm, so that the images are grouped upon the similarity in the features. This allows for obtaining a subset of the entire dataset to be compared with the query image. This subset can also be classified as the output, but for a more stringent search, the subset will be passed through the siamese network present in the next step. 

### Siamese Network <a name="siamese-network"></a>

Siamese Networks(SNN) are one shot classifiers that uses the same neural network with the same weights on two different inputs with a similarity score as the output. 

Their use cases involve comparing handwritten checks, face recognition, character recognition where the dataset is too small for classification using CNNs. 

Here the SNN is used for comparing images within a cluster and assigning them a similarity score, which is based upon the closeness to the query image.

In SNN the output vector for one of the inputs is pre-computed. In this use case, the output vector is pre-computed for the query image which forms a baseline for comparison.

In the case of standard classification, an image is passed through a CNN and then dense layers with *Softmax* as the activation function in the final layer. The output is the probabilities of the image belonging to a particular class. 

For training this type of classifier, large amounts of data is required for each class, just for training the model. For each new class introduced the classifier model has to be retrained.

This type of classifier can be used where the number of classes do not vary a lot over time and adequate training data is available. For example, identifying the breed of a cat, or birdsong, etc.

One Shot Classifiers do not require a large amount of training data, just enough to cover all use cases. The output vectors are computed and then compared. One set of output vectors can be pre-computed to give a baseline. 

A similarity score is obtained after comparing the output vectors. Triplet Loss, Euclidean or Hamming distance can be used for computing the similarity score.

| ![Siamese Network model](./output/SiameseNetwork/seq_model_plot.png) |
|:--:|
| *Siamese Sequential Model* |

| ![Complete Model](./output/SiameseNetwork/sm_model_plot.png) |
|:--:|
| *Comparison Network* |

> Note: This classifier does not assign a class label directly. It instead looks for the images which are most similar to the query image.

## Dataset <a name="dataset"></a>

The datasets for training the model currently consists of the following classes,
```
Mountain Peaks
Mountains
Snow
```

The data was collected using *Flickr* API and Python. for every class 1000 images were downloaded. Images not relevant to the class were being manually removed by going through each picture.

*Dalias* is removed as the approximately half of the pictures were of people.

*Mountain Peaks*, *Mountains* have a lot of common images between them. If necessary, they can be joined together, to create one class.

*Snow* is a mixture of snom-laden mountains, houses, and streets. 

The images can be found in the *[ImageSearch](ImageSearch)* folder.

## Updates <a name="updates"></a>

1. Created Jupyter notebook **[CNN_feature_extraction](CNN_feature_extraction.ipynb)** for extracting the features of images from the *training* and *validation* dataset. The *test* dataset was not used as this will be used to test the search engine[Unseen data]. 
2. <s>Created Jupyter notebook **[Autoencoder_relevant_features](Autoencoder_relevant_features.ipynb)** for extracting the relevant features from the feature vectors generated by the CNN. There are 2 versions, one using *Dense* layers and the other using *Conv2D* layers.<s>
3. *MobileNet* had to be dropped as the size of the feature vectors produced is too large too fit into memory even with a data generator. Will try experimenting with the *LeNet* architecture or make my own vanilla CNN.
4. **MobileNetV2** with image size of *(96,96,3)* is used for extracting the features from the training and validation datasets. Dense neural networks are used in the Autoencoder model as saving the features as 3D arrays in CSV files is infeasible at the moment. The encoded values for the training and validation datasets is available in the *[output](#output)* directory in the file **encoded_values_train_val.csv**.

## Directories <a name="directories"></a>

### config <a name="config"></a>

This *[config](config)* directory contains the configuration file required for accessing the datasets. It basically stores all the paths to the various datasets.

### dataset <a name="dataset-train-test-val"></a>

The *[dataset](dataset)* directory has the CSV files which contains the training, validation and testing split of the dataset. The CSV files helps in reading the images from the dataset without having to replicate the images.

### output <a name="output"></a>

The *[output](output)* directories contains all the output files from the various notebooks run in training the Image Search Engine.

It is split into directories each covering a stage of the training and prediction process.


## Setup <a name="setup"></a>

## Authors <a name="authours"></a>

* [*Gotam Dahiya*](https://gitlab.com/GotamDahiya)