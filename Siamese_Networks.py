'''
Sequential Model for Siamese Network.
'''
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import os, cv2, json

from config import config
from PIL import Image
from IPython.display import SVG
from tensorflow.keras.utils import plot_model, model_to_dot
from sklearn.utils import class_weight

from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import (Add, Input, Conv2D, Dense, MaxPooling2D, UpSampling2D, Activation, Lambda)
from tensorflow.keras.optimizers import Adam, SGD, Adadelta
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint, Callback
from tensorflow.keras import backend as K

from tensorflow.keras.initializers import *
from tensorflow.keras.regularizers import *
os.environ["CUDA_VISIBLE_DEVICES"]="-1"


class SiameseNetwork():

    def __init__(self):

        # tf.logging.set_verbosity(tf.logging.ERROR)

        self.input_shape = 64

        self.n1 = 32
        self.n2 = 16
        self.n3 = 8
        self.n4 = 4
        pass

    def initialize_weights(self, shape, name=None):

        return np.random.normal(loc=0.0, scale=1e-2, size=shape)
        pass

    def siamese_network(self, search, query):
        
        # Stage 1: Siamese Network Input
        X_Input = Input(self.input_shape,name="input")
         
        # Stage 2: Dense => Relu
        X = Dense(self.n1, name="dense_1")(X_Input)
        X = Activation("relu", name="relu_dense_1")(X)

        # Stage 3
        X = Dense(self.n2, name='dense_2')(X)
        X = Activation("relu", name="relu_dense_2")(X)

        # stage 4
        X = Dense(self.n3, name='dense_3')(X)
        X = Activation("relu", name="relu_dense_3")(X)

        # stage 5
        X = Dense(self.n4, name="dense_4")(X)
        X = Activation("relu", name="relu_dense_4")(X)

        seq_dense_model = [
            Dense(self.n1, name="dense_1", kernel_regularizer=l2(2e-4)),
            Activation("relu", name="relu_dense_1"),
            Dense(self.n2, name="dense_2", kernel_regularizer=l2(2e-4)),
            Activation("relu", name="relu_dense_2"),
            Dense(self.n3, name="dense_3", kernel_regularizer=l2(2e-4)),
            Activation("relu", name="relu_dense_3"),
            Dense(self.n4, name="dense_4", kernel_regularizer=l2(2e-4)),
            Activation("relu", name="relu_dense_4")
        ]

        seq_model = Sequential(seq_dense_model)

        search_input = Input(shape= self.input_shape)
        query_input  = Input(shape=self.input_shape)

        search_output = seq_model(search_input)
        query_output  = seq_model(query_input)

        distance_euclid = Lambda( lambda tensors: K.abs(tensors[0] - tensors[1]))([search_output, query_output])

        outputs = Dense(1, activation="sigmoid")(distance_euclid)

        model = Model([search_input,query_input], outputs)

        return model, seq_model
        pass

if __name__ == "__main__":

    encoded_values = pd.read_csv(config.ENCODER_VALUES_PATH,header=None)
    print(encoded_values.loc[:1,1:])
    sm = SiameseNetwork()
    model, seq_model = sm.siamese_network(encoded_values.loc[0,1:],encoded_values.loc[1,1:])

    plot_model(model,to_file="sm_model_plot.png",show_shapes=True,show_layer_names=True)
    SVG(model_to_dot(model).create(prog='dot',format='svg'))

    print(model.summary())

    plot_model(seq_model, to_file="seq_model_plot.png", show_shapes=True, show_layer_names=True)
    SVG(model_to_dot(model).create(prog='dot', format='svg'))

    print(seq_model.summary())
    