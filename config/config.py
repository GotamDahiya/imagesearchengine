'''File for storing the various variable which will be used all through out the project.

This was done with so that these variables do not have to be initialised in every new notebook/script.

ORIG_INPUT_DATASET -> directory where the dataset is stored.

BASE_PATH -> directory for storing the training, validation and testing datasets.

TRAIN, VAL, TEST -> self-explanatory. Stored in BASE_PATH

LE_PATH -> Path to JSON object containing the class binarizer

BATCH_SIZE -> Number of images in one batch

MODEL_PATH -> where to store the trained model.

BASE_SCV_PATH -> directory to store the extracted features.

AUTOENCODER_MODEL_PATH, ENCODER_MODEL_PATH, DECODER_MODEL_PATH -> Path to autoencoder, encoder and decoder models present in the output folder

ENCODER_VALUES_PATH -> Path to encoded values of training and validation.

KMEANS_CLUSTER_PATH -> CSV file for the cluster label and distance from centroid

KMEANS_MODEL_PATH -> Pickle file for the KMeans model
'''

# imporing the necessary libraries
import os

ORIG_INPUT_DATASET = "ImageSearch"

BASE_PATH = "dataset"

TRAIN = "train.csv"
TEST = "test.csv"
VAL = "validation.csv"

CLASSES = ['mountain-peaks','mountains','snow']

BATCH_SIZE = 32

LABEL_MAP = os.path.join(BASE_PATH,"label_map.pkl")
BASE_CSV_PATH = "output"

AUTOENCODER_MODEL_PATH = os.path.join("output","Autoencoders","autoencoder_model.h5")
ENCODER_MODEL_PATH = os.path.join("output","Autoencoders","encoder_model.h5")
DECODER_MODEL_PATH = os.path.join("output","Autoencoders","decoder_model.h5")

ENCODER_VALUES_PATH = os.path.join("output","Autoencoders","encoded_values_train_val.csv")

KMEANS_CLUSTER_PATH = os.path.join("output","Cluster","cluster_filenames.csv")

KMEANS_MODEL_PATH = os.path.join("output","Cluster","kmeans_model.pkl")